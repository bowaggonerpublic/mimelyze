#!/usr/bin/python3

# This program opens a given list of MIME-encoded emails
# and attempts to analyze their structure, printing
# information about them.
#
# Author: Bo Waggoner, 2017
# This software is free.

import sys
import os
import email
import string
import random

usage_str = """
When run from the command line, reads filenames from stdin
and prints out a list of most common email structures.
Example usage:
   find ~/mail/ -type f | python3 mimelyze.py

Suggestion: in python3 REPL (i.e. what you get whe running python with
no arguments from the command line), try the following (modifying your
mail directory as appropriate):

   import mimelyze
   filelist = mimelyze.get_all_filenames_under("/home/username/mail/")
   sdata = mimelyze.StructData()
   sdata.load(filelist)   # takes a while

   # to see top 20 most popular structure types
   sdata.print_msg_structs(max_num=20)
   
   # to see two random emails from the 3rd-most-popular structure type
   sdata.print_sample(3, num=2)

   # to see the most popular "components" of message structures
   sdata.print_msg_components(max_num=20)
   
"""

# appearance settings
indent_spaces = 4
attach_str = "attachment(s)"
image_str = "image(s)"


# just a convenience function to get a list of all files in a directory,
# recursively, that don't start with a .
def get_all_filenames_under(dirname, mylist=None):
  if dirname[-1] != "/":
    dirname += "/"
  if mylist is None:
    mylist = []
  for f in os.listdir(dirname):
    if f[0] == ".":
      continue
    path = dirname + f
    if os.path.isdir(path):
      get_all_filenames_under(path, mylist)
    else:
      mylist.append(path)
  return mylist

# return True iff part is an attachment part of an email
def is_attachment(part):
  cd = part.get_content_disposition()
  return cd is not None and cd.lower() == "attachment"


# return True iff part is an image part of an email
def is_image(part):
  return part.get_content_maintype() == "image"


# a representation of the structure of an email
class MsgStruct:
  def __init__(self, msg):
    self.content_type = ""   # string describing the message content
    self.subparts = []        # list of message sub-parts
    self.string = ""          # describes entire msg including subparts
    self.is_multipart = False
    self.load(msg)

  # load self and subparts recursively from 'msg'
  # msg is either attach_str, image_str or a python email object
  def load(self, msg):
    if msg == attach_str or msg == image_str:
      self.content_type = msg
      self.subparts = []
      self.is_multipart = False
      self.string = str(self)
      return

    self.content_type = msg.get_content_type()
    self.is_multipart = msg.is_multipart()
    self.subparts = []
    if msg.is_multipart():
      parts = msg.get_payload()
      ind = 0
      while ind < len(parts):
        if is_attachment(parts[ind]):
          while ind < len(parts) and is_attachment(parts[ind]):
            ind += 1
          self.subparts.append(MsgStruct(attach_str))
        elif is_image(parts[ind]):
          while ind < len(parts) and is_image(parts[ind]):
            ind += 1
          self.subparts.append(MsgStruct(image_str))
        else:
          self.subparts.append(MsgStruct(parts[ind]))
          ind += 1
    self.string = str(self)

  def __cmp__(self, other):
    return cmp(self.string, other.string)

  def __eq__(self, other): return self.string == other.string
  def __lt__(self, other): return self.string < other.string
  def __le__(self, other): return self.string <= other.string
  def __gt__(self, other): return self.string > other.string
  def __ge__(self, other): return self.string <= other.string

  def __hash__(self):
    return hash(self.string)

  def __repr__(self):
    return str(self)

  def __str__(self):
    if len(self.subparts) > 0:
      return "".join([self.content_type, " ["] + list(map(str,self.subparts)) + ["]"])
    else:
      return self.content_type

  # s_to_count maps structures to "number of that structure seen"
  # but each time we see a structure, add it 'num_per' number of times 
  # (because this message structure might represent many emails)
  # recurse: call this function on subparts as well if present
  def recur_add_to_counts(self, s_to_count, num_per, recurse=False):
    if self in s_to_count:
      s_to_count[self] += num_per
    else:
      s_to_count[self] = num_per
    if recurse:
      for p in self.subparts:
        p.recur_add_to_counts(s_to_count, num_per, recurse=True)

  # add a "pretty-printed" description of this structure
  # to a string list "slist"
  # indented according to level
  # where this is the #rank structure printed
  # and there are count of this structure
  def pretty_slist(self, slist, rank, count, level=0):
    if level == 0:
      slist.append(str(rank) + ". " + str(count))
      slist.append("\n")
    slist.append(" "*(indent_spaces*level))
    slist.append(self.content_type)
    slist.append("\n")
    for p in self.subparts:
      p.pretty_slist(slist, rank, count, level+1)

  # like pretty_slist, except that it uses a "naming schem" for structures
  # where it assumes all top-level structures are in struct_to_name
  # and lower-level structures should print their name if they are in it
  def pretty_comp_slist(self, slist, rank, count, struct_to_name, level=0):
    if level == 0:
      slist += [str(rank), ". ", struct_to_name[self], " (", str(count), ")\n"]
    slist.append(" "*(indent_spaces*level))
    if self in struct_to_name and level > 0:
      slist.append(struct_to_name[self])
      slist.append("\n")
    else:
      slist.append(self.content_type)
      slist.append("\n")
      for p in self.subparts:
        p.pretty_comp_slist(slist, rank, count, struct_to_name, level+1)


# generate unique names for the parts
# kind of like LZW compression...
def get_s_to_name(prlist):
  charset = string.ascii_uppercase
  next_int = 0
  next_name = charset[next_int]
  struct_to_name = {}
  for s,count in prlist:
    if s in struct_to_name:
      continue
    # use next_name and generate the 'next' one
    struct_to_name[s] = next_name
    next_int += 1
    i = next_int
    next_name = ""
    while i > 0:
      myi = i % len(charset)
      next_name = charset[myi] + next_name
      if i == 0:
        break
      i = int(i/len(charset))
  return struct_to_name


# given a list of (structure, number_of_emails_having_that_structure)
# return a list of pairs (structure, number_of_times_structure_occurs_anywhere)
# sorted from most to least
# here "anywhere" means to include internally within a larger email structure
def get_recur_prlist(struct_count_list):
  s_to_count = {}  # map structure to count
  for s,num_per in struct_count_list:
    s.recur_add_to_counts(s_to_count, num_per, recurse=True)
  # sort highest count to lowest
  return sorted(s_to_count.items(), key = lambda pr: (-pr[1], pr[0]))


# this class reads in a bunch of emails and stores data about their structures
# this is useful if you want to ask multiple questions, i.e.
# in the python REPL, import this file and call load() on some list of files,
# then ask multiple questions
class StructData:
  def __init__(self):
    # map string from get_structure() to pair
    # (number with this structure, list of filenames)
    self.struct_to_count_and_files = {}
    # list (structure, (count, filenames)) most common to least
    self.pairlist = []

  def load(self, filelist):
    self.struct_to_count_and_files = {}
    if len(filelist) == 0:
      return
    
    # loading might take a while for many emails, so print progress reports
    n = len(filelist)
    step = max(int(n/10), 1)
    rem = n % step
    # for each mime file:
    for i,f in enumerate(filelist):
      if i > 0 and i % step == n % step:
        print(str(int(100*i/n)) + "% done (" + str(i) + " of " + str(n) + ")")
      try:
        with open(f, encoding='utf-8', errors='replace') as myfile:
          py_msg = email.message_from_file(myfile)
          s = MsgStruct(py_msg)
          if s in self.struct_to_count_and_files:
            self.struct_to_count_and_files[s][0] += 1
            self.struct_to_count_and_files[s][1].append(f)
          else:
            self.struct_to_count_and_files[s] = [1, [f]]
      except:
        print("Error opening or processing " + f)
        raise
    print("\n")
    self.pairlist = sorted(self.struct_to_count_and_files.items(),
                           key=lambda pr: (-pr[1][0], pr[0]))

  # return list of a random subset of num filenames
  # having the structure of 'rank' in self.pairlist
  # useful if you want to get a sample of messages with a given structure
  def sample(self, rank, num=1):
    index = rank-1
    return random.sample(self.pairlist[index][1][1], min(num, len(self.pairlist[index][1][1])))

  # Note: assumes rank is 1-indexed, like the printed out version!
  def print_sample(self, rank, num=1):
    flist = self.sample(rank, num)
    for fname in flist:
      with open(fname) as f:
        print(f.read())
      print("\n---------------------------\n")

  # print out the message structures, from most to least popular, and their
  # counts. Print all if max_num == 0.
  def print_msg_structs(self, max_num=50):
    slist = []  # string list
    limit_list = self.pairlist[:max_num] if max_num >= 1 else self.pairlist
    for i,pr in enumerate(limit_list):
      s,(c,flist) = pr    # structure, (count, filelist)
      s.pretty_slist(slist, i+1, c)
      slist.append("\n")
    print("".join(slist))

  # different take. Identify "components" (common structures) and print
  # them by popularity and note when they appear as sub-structures of
  # larger structures
  def print_msg_components(self, max_num=50):
    slist = []
    comp_prlist = get_recur_prlist([(s,c) for s,(c,l) in self.pairlist])
    struct_to_name = get_s_to_name(comp_prlist)
    if max_num >= 1:
      comp_prlist = comp_prlist[:max_num]
    for i,pr in enumerate(comp_prlist):
      s, c = pr
      s.pretty_comp_slist(slist, i+1, c, struct_to_name)
      slist.append("\n")
    print("".join(slist))



if __name__ == "__main__":
  if len(sys.argv) > 1:
    print(usage_str)
    exit(0)
  sdata = StructData()
  sdata.load([f[:-1] for f in sys.stdin.readlines()])
  sdata.print_msg_structs()


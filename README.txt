mimelyze is a simple python 3 program for analyzing
the structures of a dataset of MIME-format files.

Run "python3 mimelyze.py -h" for usage instructions.

--------

EXAMPLE.
I have ~100,000 emails taking ~7.5GB (on an SSD).
The following takes ~6 minutes to run and produces (truncated here)
the most popular "structures" along with number of occurences.

$ find ~/mail/ -type f | python3 mimelyze.py

1. 58695
multipart/alternative
    text/plain
    text/html

2. 19934
text/plain

3. 7902
text/html

4. 7748
multipart/mixed
    multipart/alternative
        text/plain
        text/html
    text/plain

5. 2722
multipart/mixed
    multipart/alternative
        text/plain
        text/html
    attachment(s)

